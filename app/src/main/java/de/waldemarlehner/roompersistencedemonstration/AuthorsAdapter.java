package de.waldemarlehner.roompersistencedemonstration;

import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.renderscript.ScriptGroup;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;


import de.waldemarlehner.roompersistencedemonstration.database.Author;
import de.waldemarlehner.roompersistencedemonstration.database.AuthorAndBookDatabase;
import de.waldemarlehner.roompersistencedemonstration.database.AuthorWithBooks;
import de.waldemarlehner.roompersistencedemonstration.database.Book;

public class AuthorsAdapter extends RecyclerView.Adapter<AuthorsAdapter.ViewHolder> {

    private List<Author> authors;
    private final AuthorAndBookDatabase db;
    private final Context callee;

    public AuthorsAdapter(Context callingCtx, AuthorAndBookDatabase db){
        callee = callingCtx;
        this.db = db;
        this.updateContext();
    }

    public void updateContext(){
        this.authors = this.db.authorDao().getAllAuthors();
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View contactView = inflater.inflate(R.layout.fragment_author, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Author author = this.authors.get(position);
        holder.textView.setText(author.name);
        holder.dialogButton.setOnClickListener( v -> {

            List<Book> booksByAuthor = this.db.authorDao().getBooksFromAuthor(author.authorId).books;
            StringBuilder sb = new StringBuilder();
            sb.append("All Books:\n");
            for(Book b : booksByAuthor){
                sb.append(b.isbn).append(" : ").append(b.name).append("\n");
            }
            if(sb.length() > 0){
                sb.setLength(sb.length() - 1);
            }


            //Create Popup
            AlertDialog alert = new AlertDialog.Builder(callee)
                    .setTitle(author.name)
                    .setMessage(sb.toString())
                    .setPositiveButton("OK", null)
                    .create();
            alert.show();

        });
        holder.deleteButton.setOnClickListener( v -> {
            Toast.makeText(this.callee, "Delete", Toast.LENGTH_LONG).show();
            this.db.authorDao().delete(author);
            this.updateContext();
        });
    }

    @Override
    public int getItemCount() {
        return this.authors.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView textView;
        public FloatingActionButton dialogButton;
        public FloatingActionButton deleteButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.authorname);
            dialogButton = itemView.findViewById(R.id.btnauthorMoreInfo);
            deleteButton = itemView.findViewById(R.id.btnAuthorDelete);
        }
    }
}
