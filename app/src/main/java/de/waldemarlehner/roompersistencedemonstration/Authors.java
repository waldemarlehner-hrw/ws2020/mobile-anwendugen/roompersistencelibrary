package de.waldemarlehner.roompersistencedemonstration;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import de.waldemarlehner.roompersistencedemonstration.database.Author;

public class Authors extends AppCompatActivity {

    RecyclerView recyclerView;
    AuthorsAdapter authorsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authors);
        this.recyclerView = findViewById(R.id.booksList);
        this.recyclerView.setHasFixedSize(false);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(Authors.this));
        this.authorsAdapter = new AuthorsAdapter( this,((RoomPersistenceDemonstrationApp) getApplication()).database);
        this.recyclerView.setAdapter(this.authorsAdapter);
    }

    public void updateRecycleView(){
        this.authorsAdapter.updateContext();
    }

    public void addAuthor(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        EditText input = new EditText(builder.getContext());
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setTitle("Add Author");
        builder.setNegativeButton("Cancel", ((dialog, which) -> dialog.cancel()));
        builder.setPositiveButton("Add", ((dialog, which) -> {
            if(input.getText().toString().length() > 0){
                Author newAuthor = new Author();
                newAuthor.name = input.getText().toString();
                ((RoomPersistenceDemonstrationApp)getApplication()).database.authorDao().insert(newAuthor);
                this.updateRecycleView();
            }else{
                Toast.makeText(this, "Could not add because the name was empty.", Toast.LENGTH_LONG).show();
            }
        }));
        builder.show();
    }
}