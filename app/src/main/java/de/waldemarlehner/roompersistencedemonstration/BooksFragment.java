package de.waldemarlehner.roompersistencedemonstration;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BooksFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BooksFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String BOOK_NAME = "name";
    private static final String BOOK_ISBN = "isbn";
    private static final String BOOK_ID = "id";

    // TODO: Rename and change types of parameters
    private String bookName;
    private String bookIsbn;
    private int bookId;

    public BooksFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static BooksFragment newInstance(String bookName, String bookISBN, int bookId) {
        BooksFragment fragment = new BooksFragment();
        Bundle args = new Bundle();
        args.putString(BOOK_NAME, bookName);
        args.putString(BOOK_ISBN, bookISBN);
        args.putInt(BOOK_ID, bookId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            bookName = getArguments().getString(BOOK_NAME);
            bookIsbn = getArguments().getString(BOOK_ISBN);
            bookId = getArguments().getInt(BOOK_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_books, container, false);
    }
}