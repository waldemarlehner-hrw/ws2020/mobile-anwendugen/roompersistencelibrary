package de.waldemarlehner.roompersistencedemonstration;

import android.app.Application;
import android.widget.Toast;

import androidx.room.Room;

import de.waldemarlehner.roompersistencedemonstration.database.AuthorAndBookDatabase;

// This Class get initialized as a Singleton by Android.
public class RoomPersistenceDemonstrationApp extends Application {

    AuthorAndBookDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();
        this.database = Room.databaseBuilder(getApplicationContext(), AuthorAndBookDatabase.class, "authorAndBookDatabase").allowMainThreadQueries().build();
    }
}
