package de.waldemarlehner.roompersistencedemonstration;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.view.View;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void changeToAuthors(View v){
        startActivity(new Intent(MainActivity.this, Authors.class));
    }
    public void changeToBooks(View v){
        startActivity(new Intent(MainActivity.this, Books.class));
    }
    public void changeToJunction(View v){
        startActivity(new Intent(MainActivity.this, Junction.class));
    }



}