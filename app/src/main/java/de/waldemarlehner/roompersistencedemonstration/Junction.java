package de.waldemarlehner.roompersistencedemonstration;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import de.waldemarlehner.roompersistencedemonstration.database.Author;
import de.waldemarlehner.roompersistencedemonstration.database.AuthorAndBookDatabase;
import de.waldemarlehner.roompersistencedemonstration.database.AuthorToBook;
import de.waldemarlehner.roompersistencedemonstration.database.Book;

public class Junction extends AppCompatActivity {

    AuthorAndBookDatabase db;
    Spinner authorSpinner, bookSpinner;
    CheckBox islinked;

    Book selectedBook = null;
    Author selectedAuthor = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_junction);
        this.db = ((RoomPersistenceDemonstrationApp)getApplication()).database;
        this.authorSpinner = findViewById(R.id.spinnerAuthor);
        this.bookSpinner = findViewById(R.id.spinnerBook);
        this.islinked = findViewById(R.id.isBookAuthorLinked);
        this.setupSpinners();
        this.setupCheckbox();

    }

    private void setupCheckbox() {
        this.islinked.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(this.selectedAuthor != null && this.selectedBook != null){
                AuthorToBook atb = new AuthorToBook();
                atb.authorId = selectedAuthor.authorId;
                atb.bookId = selectedBook.bookId;
                if(isChecked){
                    this.db.authorToBookDao().insert(atb);
                }else{
                    this.db.authorToBookDao().delete(atb);
                }
            }
        });
    }


    private void setupSpinners(){
        List<Author> authors = db.authorDao().getAllAuthors();
        ArrayAdapter<Author> authorsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, authors);
        List<Book> books = db.bookDao().getAllBooks();
        ArrayAdapter<Book> booksAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, books);
        authorSpinner.setAdapter(authorsAdapter);
        bookSpinner.setAdapter(booksAdapter);


        authorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Author author = (Author)authorSpinner.getSelectedItem();
                if(author != null){
                    selectedAuthor = author;
                }
                updateCheckBox();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });

        bookSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Book book = (Book)bookSpinner.getSelectedItem();
                if(book != null){
                    selectedBook = book;
                }
                updateCheckBox();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

    }

    private void updateCheckBox(){
        if(this.selectedBook != null && this.selectedAuthor != null){
            this.islinked.setEnabled(true);
            boolean link = db.authorToBookDao().doesLinkExist(this.selectedAuthor.authorId, this.selectedBook.bookId);
            this.islinked.setChecked(link);
        }else{
            this.islinked.setEnabled(false);
            this.islinked.setChecked(false);
        }
    }

}