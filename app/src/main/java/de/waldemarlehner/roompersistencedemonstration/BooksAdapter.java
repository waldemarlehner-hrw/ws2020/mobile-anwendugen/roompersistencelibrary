package de.waldemarlehner.roompersistencedemonstration;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import de.waldemarlehner.roompersistencedemonstration.database.Author;
import de.waldemarlehner.roompersistencedemonstration.database.AuthorAndBookDatabase;
import de.waldemarlehner.roompersistencedemonstration.database.Book;

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.ViewHolder> {

    private List<Book> books;
    private final AuthorAndBookDatabase db;
    private final Context callee;

    public BooksAdapter(Context callingCtx, AuthorAndBookDatabase db){
        callee = callingCtx;
        this.db = db;
        this.updateContext();
    }

    public void updateContext(){
        this.books = this.db.bookDao().getAllBooks();
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View contactView = inflater.inflate(R.layout.fragment_author, parent, false);
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Book book = this.books.get(position);
        holder.textView.setText(book.name);
        holder.dialogButton.setOnClickListener( v -> {

            List<Author> authorsByBook = this.db.bookDao().getAuthorsFromBook(book.bookId).authors;
            StringBuilder sb = new StringBuilder();
            sb.append("All Authors:\n");
            for(Author b : authorsByBook){
                sb.append(b.name).append(" : ").append("\n");
            }
            if(sb.length() > 0){
                sb.setLength(sb.length() - 1);
            }

            //Create Popup
            AlertDialog alert = new AlertDialog.Builder(callee)
                    .setTitle(book.name)
                    .setMessage(sb.toString())
                    .setPositiveButton("OK", null)
                    .create();
            alert.show();

        });
        holder.deleteButton.setOnClickListener( v -> {
            Toast.makeText(this.callee, "Delete", Toast.LENGTH_LONG).show();
            this.db.bookDao().delete(book);
            this.updateContext();
        });
    }

    @Override
    public int getItemCount() {
        return this.books.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView textView, isbnView;
        public FloatingActionButton dialogButton;
        public FloatingActionButton deleteButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.authorname);
            isbnView = itemView.findViewById(R.id.bookISBN);
            dialogButton = itemView.findViewById(R.id.btnauthorMoreInfo);
            deleteButton = itemView.findViewById(R.id.btnAuthorDelete);
        }
    }
}
