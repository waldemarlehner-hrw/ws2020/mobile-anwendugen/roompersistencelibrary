package de.waldemarlehner.roompersistencedemonstration.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

@Dao
public interface BookDao {
    @Insert
    void insert(Book book);

    @Delete
    void delete(Book book);

    @Query("SELECT * from book")
    List<Book> getAllBooks();

    @Query("SELECT * FROM book WHERE name LIKE '%' || :nameLike || '%'")
    List<Book> getBooksByName(String nameLike);

    @Query("SELECT * FROM book WHERE isbn LIKE '%' || :isbnLike || '%'")
    List<Book> getBooksByISBN(String isbnLike);

    @Transaction
    @Query("SELECT * FROM Book WHERE bookId = :bookId")
    BookWithAuthors getAuthorsFromBook(int bookId);
}
