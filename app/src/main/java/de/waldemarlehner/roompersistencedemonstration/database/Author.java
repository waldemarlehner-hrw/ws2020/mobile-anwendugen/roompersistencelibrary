package de.waldemarlehner.roompersistencedemonstration.database;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.List;

@Entity
public class Author {
    @PrimaryKey(autoGenerate = true)
    public int authorId;

    public String name;

    @Ignore
    public String toString(){
        return name;
    }
}
