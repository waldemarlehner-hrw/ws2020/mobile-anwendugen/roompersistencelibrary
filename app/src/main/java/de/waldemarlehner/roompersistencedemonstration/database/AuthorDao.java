package de.waldemarlehner.roompersistencedemonstration.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

@Dao
public interface AuthorDao {
    @Insert
    void insert(Author author);

    @Delete
    void delete(Author author);

    @Query("SELECT * from author")
    List<Author> getAllAuthors();

    @Query("SELECT * FROM author WHERE name LIKE '%' || :nameLike || '%'")
    List<Author> getAuthorsByName(String nameLike);

    @Transaction
    @Query("SELECT * FROM Author WHERE authorId = :authorId")
    AuthorWithBooks getBooksFromAuthor(int authorId);

}
