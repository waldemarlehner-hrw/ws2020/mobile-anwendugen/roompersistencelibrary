package de.waldemarlehner.roompersistencedemonstration.database;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Book {
    @PrimaryKey(autoGenerate = true)
    public int bookId;

    public String name;

    public String isbn;

    @Ignore
    public String toString(){
        return name;
    }
}
