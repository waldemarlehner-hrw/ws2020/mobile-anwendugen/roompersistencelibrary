package de.waldemarlehner.roompersistencedemonstration.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface AuthorToBookDao {
    // In case a link between an Author and a Book already exists the call can be ignored
    // as no additional data is stored in the AuthorToBook Table.
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(AuthorToBook authorBookPair);

    @Delete
    void delete(AuthorToBook authorBookPair);

    @Query("SELECT EXISTS(SELECT * from AuthorToBook WHERE authorId = :authorId AND bookId = :bookId)")
    boolean doesLinkExist(int authorId, int bookId);
}
