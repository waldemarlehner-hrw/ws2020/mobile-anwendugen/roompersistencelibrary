package de.waldemarlehner.roompersistencedemonstration.database;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

//see https://developer.android.com/training/data-storage/room/relationships


public class AuthorWithBooks {
    @Embedded
    public Author author;
    @Relation(
            parentColumn = "authorId",
            entityColumn = "bookId",
            associateBy = @Junction(AuthorToBook.class)
    )
    public List<Book> books;
}
