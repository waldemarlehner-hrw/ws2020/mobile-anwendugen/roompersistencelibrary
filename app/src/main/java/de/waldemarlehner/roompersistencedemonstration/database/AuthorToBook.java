package de.waldemarlehner.roompersistencedemonstration.database;

import androidx.room.Entity;
import androidx.room.ForeignKey;

@Entity(primaryKeys = {"authorId", "bookId"}, foreignKeys = {
        @ForeignKey(entity = Author.class, parentColumns = "authorId", childColumns = "authorId", onDelete = ForeignKey.CASCADE),
        @ForeignKey(entity = Book.class, parentColumns = "bookId", childColumns = "bookId", onDelete = ForeignKey.CASCADE)
})
public class AuthorToBook {
    public int authorId;
    public int bookId;
}
