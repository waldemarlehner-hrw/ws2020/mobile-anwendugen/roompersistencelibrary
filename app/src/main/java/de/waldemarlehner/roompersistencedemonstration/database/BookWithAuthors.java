package de.waldemarlehner.roompersistencedemonstration.database;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

//see https://developer.android.com/training/data-storage/room/relationships
public class BookWithAuthors {
    @Embedded public Book book;
    @Relation(
            parentColumn = "bookId",
            entityColumn = "authorId",
            associateBy = @Junction(AuthorToBook.class)
    )
    public List<Author> authors;
}

