package de.waldemarlehner.roompersistencedemonstration.database;


import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database( entities = {
        // Register these Entities to the Database.
        Author.class,
        AuthorToBook.class,
        Book.class
    }, version = 2)
public abstract class AuthorAndBookDatabase extends RoomDatabase {
    public abstract AuthorDao authorDao();
    public abstract BookDao bookDao();
    public abstract AuthorToBookDao authorToBookDao();
}
