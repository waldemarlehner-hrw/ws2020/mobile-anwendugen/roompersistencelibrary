package de.waldemarlehner.roompersistencedemonstration;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import de.waldemarlehner.roompersistencedemonstration.database.AuthorAndBookDatabase;
import de.waldemarlehner.roompersistencedemonstration.database.Book;

public class Books extends AppCompatActivity {


    RecyclerView recyclerView;
    BooksAdapter booksAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books);
        this.recyclerView = findViewById(R.id.booksList);
        this.recyclerView.setHasFixedSize(false);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(Books.this));
        this.booksAdapter = new BooksAdapter(this, ((RoomPersistenceDemonstrationApp)getApplication()).database);
        this.recyclerView.setAdapter(this.booksAdapter);
    }

    public void updateRecycleView(){
        this.booksAdapter.updateContext();
    }

    public void addBook(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LinearLayout layout = new LinearLayout(builder.getContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        EditText nameInput = new EditText(builder.getContext());
        EditText isbnInput = new EditText(builder.getContext());

        builder.setView(layout);
        builder.setTitle("Add Book");
        nameInput.setInputType(InputType.TYPE_CLASS_TEXT);
        isbnInput.setInputType(InputType.TYPE_CLASS_NUMBER);
        nameInput.setHint("Book Name");
        isbnInput.setHint("ISBN");
        layout.addView(nameInput);
        layout.addView(isbnInput);

        builder.setNegativeButton("Cancel", ((dialog, which) -> {
            dialog.cancel();
        }));
        builder.setPositiveButton("Add", ((dialog, which) -> {
            if(nameInput.getText().length() > 0){
                Book book = new Book();
                book.name = nameInput.getText().toString();
                book.isbn = isbnInput.getText().toString();
                AuthorAndBookDatabase db = ((RoomPersistenceDemonstrationApp)getApplication()).database;
                db.bookDao().insert(book);
                this.updateRecycleView();
            }else{
                Toast.makeText(this, "Could not add because the name was empty.", Toast.LENGTH_LONG).show();
            }
        }));
        builder.show();

    }
}